<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class='jumbotron' id="page_about" style="background-position:center center; background-image: url(<?php the_field('background_image'); ?>);">
      <h1><?php the_title(); ?></h1>
    </div>

    <section class='welcome' id='attorney_page'>
                
      <div class='container'>
          
        <div class="attorney_overview">
            
            <div class="container">
                <h1 class="headline"><?php the_field('jill_name'); ?></h1>
                <hr class="style1">
            </div>
            
            <div class="attorney_img" style="background-image: url(<?php the_field('jill_photo'); ?>);">
                <h3 id="show_jill">About Jill</h3>
            </div>
            
        

            <div class="attorney_bio" id="jill_bio">
                <?php the_field('jill_bio'); ?>
                
            </div>
        </div>
          
                  <div class="attorney_overview">
            
            <div class="container">
                <h1 class="headline"><?php the_field('james_name'); ?></h1>
                <hr class="style1">
            </div>
            
            <div class="attorney_img" style="background-image: url(<?php the_field('james_photo'); ?>);">
                <h3 id="show_james">About James</h3>
            </div>
            
        

            <div class="attorney_bio" id="james_bio">
                <?php the_field('james_bio'); ?>
            </div>
        </div>
          
        <div class="attorney_overview">
            
            <div class="container">
                <h1 class="headline"><?php the_field('ted_name'); ?></h1>
                <hr class="style1">
            </div>
            
            <div class="attorney_img" style="background-image: url(<?php the_field('ted_photo'); ?>);">
                <h3 id="show_ted">About Ted</h3>
            </div>
            
        

            <div class="attorney_bio" id="ted_bio">
                <?php the_field('ted_bio'); ?>
            </div>
        </div>
          
        <div class="attorney_overview">
            
            <div class="container">
                <h1 class="headline"><?php the_field('andrew_name'); ?></h1>
                <hr class="style1">
            </div>
            
            <div class="attorney_img" style="background-position:center center; background-image: url(<?php the_field('andrew_photo'); ?>);">
                <h3 id="show_andrew">About Andrew</h3>
            </div>
            
        

            <div class="attorney_bio" id="andrew_bio">
                <?php the_field('andrew_bio'); ?>
            </div>
        </div>
          

      </div>
    </section>


<div class="areaofexpertise" id="attorney_page">
  <div class="container">
    <h1>Areas of Practice</h1>
    <hr class="style1">
  </div>
    <div class="container area">

        <div class="row areas_expertise">
              <div class="col-xs-12 col-md-4">
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/family_law.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
                  
                      
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
              <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/real_estate_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('real_estate_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
               <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/bankruptcy_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('bankruptcy_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
        </div>
        

    </div>
        <div class="clear_row">
        
            <a class="btn btn-primary btn-lg view_btn" href="/practice-areas/" role="button" tabindex="0">View All Areas</a>
        </div>

   
<script>
$("#show_jill").click(function(){
    $(".attorney_bio#jill_bio").slideToggle();
});    
    $("#show_james").click(function(){
    $(".attorney_bio#james_bio").slideToggle();
});  
    $("#show_ted").click(function(){
    $(".attorney_bio#ted_bio").slideToggle();
});  
    $("#show_andrew").click(function(){
    $(".attorney_bio#andrew_bio").slideToggle();
});  
</script>


<?php
get_footer();
