<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class='jumbotron' id="page_about" style="background-position:center center; background-image: url(<?php the_field('banner'); ?>);">
      <h1><?php the_title(); ?></h1>
    </div>

    <section class='welcome'>
      <div class='container'>
            <div class="container">
                <h1 class="headline">Welcome to the Park and Caporrino</h1>
                <hr class="style1">
            </div>
<?php the_field('content'); ?>
<!--
        <p>The experienced lawyers at the law firm of Park & Caporrino, LLP provide quality legal services in the areas of Bankruptcy, Real Estate, Civil Litigation, Real Estate (including Landlord/Tenant), Family Law (including Divorce and Custody Disputes and Domestic Violence), Commercial Litigation, Business Law, DWI and other Motor Vehicle Offenses, Expungement and Juvenile Delinquency.</p>

        <p>Our staff takes the time to examine all of the details of your legal needs in an effort to provide you with big firm expertise and small firm individual attention.</p>

        <p>The firm’s office, in Leonia, New Jersey, is just minutes from Rte. 80, Rte. 4, the New Jersey Turnpike and the George Washington Bridge.</p>
-->
      </div>
    </section>


<div class="areaofexpertise" id="">
  <div class="container">
    <h1>Areas of Practice</h1>
    <hr class="style1">
  </div>
    <div class="container area">

        <div class="row areas_expertise">
              <div class="col-xs-12 col-md-4">
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/family_law.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
                  
                      
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
              <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/real_estate_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('real_estate_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
               <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/bankruptcy_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('bankruptcy_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
        </div>
        

    </div>
        <div class="clear_row">
        
            <a class="btn btn-primary btn-lg view_btn" href="/practice-areas/" role="button" tabindex="0">View All Areas</a>
        </div>

   
   
  


<?php
get_footer();

