<?php
/*
 * Template Name: News Template
 */

get_header(); ?>

<div class="article_page_container">


<div class="tab_sidebar">
    <ul>
        <li data-related="bankrupt">Bankruptcy</li>
        <li data-related="business">Business Law</li>
        <li data-related="civil">Civil Litigation</li>
        <li data-related="criminal">Criminal Law</li>
        <li data-related="family">Family Law</li>
        <li data-related="landlord">Landlord - Tenant Practice</li>
        <li data-related="real_estate">Real Estate</li>
        <li data-related="tickets">Tickets & DWI </li>
        <li data-related="planning">Wills & Estate Planning </li>
    </ul>
</div>
    <section class='welcome' id="blog_posts">
        <div class='container articles' id="main" style="display:block;">
            <div class="container">
                <h1 class="headline">All Articles</h1>
                <hr class="style1">
            </div>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
        
              <div class='container articles' id="bankrupt">
                <h1 class="headline">Bankruptcy News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="business">
                <h1 class="headline">Business Law News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="business-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="civil">
                <h1 class="headline">Civil Litigation News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="civil-litigation" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="criminal">
                <h1 class="headline">Criminal Law News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="criminal-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="landlord">
                <h1 class="headline">Landlord - Tenant Practice News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="domestic-violence" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="family">
                <h1 class="headline">Family Law News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>

                      <div class='container articles' id="tickets">
                <h1 class="headline">Tickets & DWI News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="juvenile-delinquency" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
                      <div class='container articles' id="planning">
                <h1 class="headline">Wills & Estate Planning News</h1>
                <hr class="style1">
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="motor-vehicle-offenses" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
           </div>
                 
        </div>
            <div class='container articles' id="real_estate">
                <h1 class="headline">Real Estate News</h1>
                <hr class="style1">
              <div class="article_post">
                <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
               </div>
          </div>

    </section>





<div class="practice_area_container">
    
<!--Bankruptcy-->
    <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/finance_thumb.jpg);">
        <a href="/practice-areas/bankruptcy/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dollar-coin-stack.png" />
                <p>Bankruptcy</p>
            </div>
        </a>
    </div>

<!--Business Law-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/business_thumb.jpg);">
        <a href="/practice-areas/business-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/briefcase.png" />
                <p>Business Law</p>
            </div>
        </a>
    </div>

<!--Civil Litigation-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/civil_thumb.jpg);">
        <a href="/practice-areas/civil-litigation/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/pyramid.png" />
                <p>Civil Litigation</p>
            </div>
        </a>
    </div>

<!--Criminal Law-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/criminal_thumb.jpg);">
        <a href="/practice-areas/criminal-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/jail.png" />
                <p>Criminal Law</p>
            </div>
        </a>
    </div>
    
<!--Family Law-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/family_law.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
    
<!--Landlord - Tenant Practice-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/landlord_thumb.jpg);">
        <a href="/practice-areas/landlord-tenant-practice/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/key.png" />
                <p>Landlord - Tenant Practice</p>
            </div>
        </a>
    </div>
    
<!--Real Estate-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/realestate_thumb.jpg);">
        <a href="/practice-areas/real-estate/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/house.png" />
                <p>Real Estate</p>
            </div>
        </a>
    </div>
    
<!--Tickets & DWI Practice-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/dwi_thumb.jpg);">
        <a href="/practice-areas/tickets-dwi/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/court.png" />
                <p>Tickets & DWI</p>
            </div>
        </a>
    </div>
    
<!--Wills & Estate Planning-->
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/estate_thumb.jpg);">
        <a href="/practice-areas/wills-estate-planning/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/pen.png" />
                <p>Wills & Estate Planning</p>
            </div>
        </a>
    </div>
    
</div>
</div>    
<script>
$(".tab_sidebar ul li").on("click", function() {
    $(".tab_sidebar ul li").removeClass('active');
    $(this).addClass('active');
    $('section#blog_posts div.container').hide();
  $("div[id=" + $(this).attr("data-related") + "]").show();
});
</script>

<?php
get_footer();
