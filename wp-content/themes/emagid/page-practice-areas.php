<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class='jumbotron' id="page_about" style="background-image: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/banner_pa_1.jpg);">
      <h1><?php the_title(); ?></h1>
    </div>
    
<section>


<div class="practice_area_container">
    
        <?php
            $args = array(
      'post_type' => 'practice_areas'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
    

    <div class="practice_area" style="background-image:url(<?php the_field('image'); ?>);">
        <a href="<?php the_field('link'); ?>">
            <div class="overlay">
<!--                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dollar-coin-stack.png" />-->
                <p><?php the_field('practice_area_name'); ?></p>
            </div>
        </a>
    </div>
    
    
    <?php
            }
                  }
            else  {
            echo 'No Practice Areas Found';
            }
      ?>


<!--
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/business_thumb.jpg);">
        <a href="/practice-areas/business-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/briefcase.png" />
                <p>Business Law</p>
            </div>
        </a>
    </div>


        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/civil_thumb.jpg);">
        <a href="/practice-areas/civil-litigation/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/pyramid.png" />
                <p>Civil Litigation</p>
            </div>
        </a>
    </div>


        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/criminal_thumb.jpg);">
        <a href="/practice-areas/criminal-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/jail.png" />
                <p>Criminal Law</p>
            </div>
        </a>
    </div>
    

        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/marriage_thumb.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
    

        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/landlord_thumb.jpg);">
        <a href="/practice-areas/landlord-tenant-practice/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/key.png" />
                <p>Landlord - Tenant Practice</p>
            </div>
        </a>
    </div>
    

        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/realestate_thumb.jpg);">
        <a href="/practice-areas/real-estate/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/house.png" />
                <p>Real Estate</p>
            </div>
        </a>
    </div>
    

        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/dwi_thumb.jpg);">
        <a href="/practice-areas/tickets-dwi/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/court.png" />
                <p>Tickets & DWI</p>
            </div>
        </a>
    </div>
    

        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/estate_thumb.jpg);">
        <a href="/practice-areas/wills-estate-planning/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/pen.png" />
                <p>Wills & Estate Planning</p>
            </div>
        </a>
    </div>
-->
    
</div>
<script>
$(".tab_sidebar ul li").on("click", function() {
    $(".tab_sidebar ul li").removeClass('active');
    $(this).addClass('active');
    $('section#tabbed_content div.container').hide();
  $("div[id=" + $(this).attr("data-related") + "]").show();
});
</script>

<?php
get_footer();


