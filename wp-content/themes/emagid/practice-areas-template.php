<?php
/*
 * Template Name: Practice Area Template
 */

get_header(); ?>

    <div class='jumbotron' id="page_about" style="background-image: url(<?php the_field('banner'); ?>);">
      <h1><?php the_title(); ?></h1>
    </div>


<div class="tab_sidebar">
    <div class="articles">
        <h4>Articles</h4>
        <div>
            <p>A New Law Awards Attorney Fees to New Jersey Residential Tenants</p>
            <button>Read More</button>
        </div>
        <div>
            <p>A New Law Awards Attorney Fees to New Jersey Residential Tenants</p>
            <button>Read More</button>
        </div>
        <div>
            <p>A New Law Awards Attorney Fees to New Jersey Residential Tenants</p>
            <button>Read More</button>
        </div>
    </div>
    <ul>
        <a href="/practice-areas/bankruptcy/">
            <li data-related="bankrupt">
                Bankruptcy
            </li>
        </a>
        
        <a href="/practice-areas/business-law/">
            <li data-related="bankrupt">
            Business Law
            </li>
        </a>
        
        <a href="/practice-areas/civil-litigation/">
            <li data-related="bankrupt">
            Civil Litigation
            </li>
        </a>
        
        <a href="/practice-areas/criminal-law/">
            <li data-related="bankrupt">
            Criminal Law
            </li>
        </a>
        
        <a href="/practice-areas/family-law/">
            <li data-related="bankrupt">
            Family Law
            </li>
        </a>
        
        <a href="/practice-areas/landlord-tenant-practice/">
            <li data-related="bankrupt">
            Landlord - Tenant Practice
            </li>
        </a>
        
        <a href="/practice-areas/real-estate/">
            <li data-related="bankrupt">
            Real Estate
            </li>
        </a>
        
        <a href="/practice-areas/tickets-dwi">
            <li data-related="bankrupt">
            Tickets & DWI
            </li>
        </a>
        
        <a href="/practice-areas/wills-estate-planning/">
            <li data-related="bankrupt">
            Wills & Estate Planning
            </li>
        </a>
    </ul>
</div>

    <section class='' id="tabbed_content">
    <div class='container' id="intro" style="display:block;">
        <?php the_field('content'); ?>
      </div>
        
        
        
        
      <div class='container' id="bankrupt">
          <h3>Bankruptcy</h3>
          <?php the_field('bankruptcy_text'); ?>
      </div>
              <div class='container' id="business">
          <h3>Business Law</h3>
          <?php the_field('business_law_text'); ?>
      </div>
              <div class='container' id="civil">
          <h3>Civil Litigation</h3>
          <?php the_field('civil_litigation_text'); ?>
      </div>
              <div class='container' id="criminal">
          <h3>Criminal Law</h3>
          <?php the_field('criminal_law_text'); ?>
      </div>
              <div class='container' id="landlord">
          <h3>Landlord - Tenant Practice</h3>
          <?php the_field('domestic_violence_text'); ?>
      </div>
              <div class='container' id="family">
          <h3>Family Law</h3>
          <?php the_field('family_law_text'); ?>
      </div>
              <div class='container' id="tickets">
          <h3>Tickets & DWI </h3>
          <?php the_field('juvenile_delinquency_text'); ?>
      </div>
              <div class='container' id="wills">
          <h3>Wills & Estate Planning </h3>
          <?php the_field('motor_vehicle_offenses_text'); ?>
      </div>
              <div class='container' id="real_estate">
          <h3>Real Estate</h3>
          <?php the_field('real_estate_text'); ?>
      </div>
    </section>




<div class="practice_area_container">
    
        <?php
            $args = array(
      'post_type' => 'practice_areas'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
    

    <div class="practice_area" style="background-image:url(<?php the_field('image'); ?>);">
        <a href="<?php the_field('link'); ?>">
            <div class="overlay">
<!--                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dollar-coin-stack.png" />-->
                <p><?php the_field('practice_area_name'); ?></p>
            </div>
        </a>
    </div>
    
    
    <?php
            }
                  }
            else  {
            echo 'No Practice Areas Found';
            }
      ?>
</div>
<script>
<script>
//$(".tab_sidebar ul li").on("click", function() {
//    $(".tab_sidebar ul li").removeClass('active');
//    $(this).addClass('active');
//    $('section#tabbed_content div.container').hide();
//  $("div[id=" + $(this).attr("data-related") + "]").show();
//});
</script>

<?php
get_footer();


