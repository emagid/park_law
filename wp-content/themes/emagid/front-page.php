<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class="jumbotron">
    <?php
            $args = array(
      'post_type' => 'home_sliders',
      'posts_per_page' => '9'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
        <div class="background" style="background-image: url(<?php the_field('image'); ?>);">
         <h1 class="display-3" id="clip"><?php the_field('title'); ?></h1>
<!--            PARK & CAPORRINO-->
            <h1 class="display-3" id="clip"><?php the_field('subtitle'); ?></h1>
<!--            We are here to help-->
        <a class="btn btn-primary btn-lg" href="<?php the_field('link'); ?>" role="button">Learn More</a>
        </div>
        
    <?php
            }
                  }
            else  {
            echo 'No Team Members Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
    </div>


<!--Slick Slider-->
<script>
$(document).ready(function(){
  $('.jumbotron').slick({
      autoplay: true,
      autoplaySpeed: 4000,
                 dots: true,
            infinite: true,
            speed: 500,
    arrows: true
  });
});
</script>

    <div class="welcome">
      
<!--      <h1><//?php the_field('welcome_title'); ?></h1>-->
      <div class="container-welcome">
        <div class="text">
            <p><?php the_field('welcome_text'); ?></p>
            <a class="btn btn-primary btn-lg" href="/firm-overview" role="button"><?php the_field('welcome_button'); ?></a>
        </div>
     </div>
    </div>




<div class="areaofexpertise">
  <div class="container">
    <h1><?php the_field('area_of_expertise'); ?></h1>
    <hr class="style1">
  </div>
    <div class="container area">

        <div class="row areas_expertise">
              <div class="col-xs-12 col-md-4">
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/family_law.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
<!--
                  <div class="inside">
                      <h3><//?php the_field('family_law_box'); ?></h3>
                  </div>
-->
                      
                                <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
              <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/real_estate_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('real_estate_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
               <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/bankruptcy_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('bankruptcy_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
        </div>
        

    </div>
        <div class="clear_row">
        
            <a class="btn btn-primary btn-lg view_btn" href="/practice-areas/" role="button" tabindex="0">View All Areas</a>
        </div>
<div class="container_test">
    
    <div class=" testimonials" id="testimonials">
            
        <div class="review_slider">
        
            <?php
            $args = array(
      'post_type' => 'testimonials'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
        
    <div class="review_block">
        <div class="review_image">
            <img src="<?php the_field('image'); ?>" alt="..." />
            <p><?php the_field('name'); ?></p>
            <img class="stars" src="<?php echo get_bloginfo('template_url'); ?>/assets/img/fivestars.png" alt="..." />
        </div>
        <div class="review_text">
            <p><?php the_field('testimonial'); ?></p>
        </div>
    </div>
            <?php
            }
                  }
            else  {
            echo 'No Reviews Found';
            }
      ?> 
    </div>
        </div>


 
    </div>


 
    </div>
    <div>

</div>

<section class="contact_section">
    <div class="contact_maps">
        <iframe class="map_nj" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3017.3390922757394!2d-73.99551838499995!3d40.864432079315556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f7254299ef25%3A0x92129109a87aca1f!2s130+Fort+Lee+Rd%2C+Leonia%2C+NJ+07605!5e0!3m2!1sen!2sus!4v1501602253390" width="100%" height="750px" frameborder="0" style="border:0" allowfullscreen></iframe>
        <iframe class="map_ny" style="display:none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.700097008235!2d-73.98761584900471!3d40.746624179226636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259a867d19a55%3A0xabd4e4b9e9208422!2s307+5th+Ave%2C+New+York%2C+NY+10016!5e0!3m2!1sru!2sus!4v1499374189405" width="100%" height="750px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>


    <div class="contact_text">
        <div class="areaofexpertise">
        

        <div class="container">
            <h1>Contact Us</h1>
            <hr class="style1">
        </div>
            
            <div class="contact_switcher">
                <div class="switch selected" id="switch_location">
                    <h4>Locations</h4>
                </div>
                <div class="switch" id="switch_contact">
                    <h4>Contact</h4>
                </div>
                
            </div>
            
            <div class="contact_area">
                    <div class="address selected" id="address_nj">
                        <p>130 Fort Lee Road<br>
                        Leonia, NJ 07605<br>
                        (201) 242-9666 (Andrew/Jill Park)<br>
                        (201) 592-7767 (James Caporrino)</p>
                    </div>
                    <div class="address" id="address_ny">
                        <p>307 5th Avenue, 4th Floor <br>
                        New York, NY 10016<br>
                        (212) 696-9494
                        </p>
                    </div>
                    
                    <div class="contact_email">
                        <p>
                        <a href="mailto:jsp@parkcaporrino.com">Jill SunJung Park – jsp@parkcaporrino.com</a></p>
                        <p>
                        <a href="mailto:JAC@PARKCAPORRINO.COM">James A. Caporrino – jac@parkcaporrino.com</a>  </p> 
                        <p>
                        <a href="mailto:AIP@PARKCAPORRINO.COM">Andrew Inbeh Park – aip@parkcaporrino.com</a></p>
                        <p>
                        <a href="mailto:TMP@PARKCAPORRINO.COM">Ted Minsung Park – tmp@parkcaporrino.com</a>     </p>            
                    </div>
                </div>
                <div class="contact_form home_contact" style="display:none;">
                <?php echo do_shortcode('[contact-form-7 id="32" title="Contact Form"]'); ?>
                </div>
            
            </div>
            
        </div>
    </div>
</section>


<script>
$(".contact_area .address").on("click", function() {
    $(".contact_area .address").removeClass('selected');
    $(this).addClass('selected');
});
    
$("div#address_ny").on("click", function() {
    $(".map_nj").hide();
    $(".map_ny").show();
});
    
$("div#address_nj").on("click", function() {
    $(".map_ny").hide();
    $(".map_nj").show();
});
    
$(".contact_switcher .switch").on("click", function() {
    $(".contact_switcher .switch").removeClass('selected');
    $(this).addClass('selected');
});
    
$("div#switch_location").on("click", function() {
    $(".contact_form").hide();
    $(".contact_addresses").show();
});
    
$("div#switch_contact").on("click", function() {
    $(".contact_addresses").hide();
    $(".contact_form").show();
});
</script>

<!--Slick Slider-->
<script>
$(document).ready(function(){
  $('.review_slider').slick({
      autoplay: true,
      autoplaySpeed: 7500,
                 dots: true,
            infinite: true,
            speed: 500,
    arrows: true
  });
});
</script>


<?php
get_footer();
