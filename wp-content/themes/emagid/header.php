<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/media.css">
    
     <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
    
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    
    <script src="js/main.js"></script>

	<?php wp_head(); ?>
</head>

<body id="home" data-spy="scroll" data-target=".navbar" data-offset="100" <?php body_class(); ?>>

                <div class="translate_bar">
                  <?php get_sidebar();?>
                    <p class="tel"> (201) 592-7767 (James Caporrino) </p>  
                    
                    <div class="float_right" id="contact_number">
                        <p>(201) 242-9666 (Andrew/Jill Park) </p>
                    </div>
            </div>

    <header class="header">

      <div class="container">
        <a href="/" class="logo"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/logo_red.png"></a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />

          	 		<?php 
		wp_nav_menu(array(
            "container_class"=>"menu",
		"theme_location" => "menu-1"
    )); ?>
      </div>
    </header>


