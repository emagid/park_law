<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
</div>
<footer>
<div class="container-fluid">
  <div class="container">
  <div class="first">
      <ul>
        <h4>About Us</h4>
	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "menu-2"
    )); ?>
      </ul>

      </div>
  <div class="second">
    <ul class="address">
        <h4>Locations</h4>
      <li>130 fort lee road<br> leonia, NJ 07605 <br>(201) 242-9666 (Andrew/Jill Park)<br>(201) 592-7767 (James Caporrino)</li><br>
      <li>307 5th avenue, 4th floor<br> New York, NY 10016<br>(212) 696-9494</li>
    </ul>

  </div>
  <div class="third">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/logowhite.png">
            <ul class="social-links">
          <li><a href="https://www.facebook.com/parkcaporrino/?ref=aymt_homepage_panel" target="_blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a></li>
          <li><a href="https://www.yelp.com/biz/park-and-caporrino-leonia" target="_blank"><i class="fa fa-yelp fa-2x" aria-hidden="true"></i></a></li>
      </ul>
  </div>
  <div class="four">
      <a href="https://www.avvo.com/attorneys/07605-nj-james-caporrino-1596394.html" target="_blank">
      <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/avvo.png">
      </a>
      <ul>
        <li>Copyright &copy; 2006-2017 Park & Caporrino - All Rights Reserved</li>
      </ul>
      
<!--
    <ul>

          <button type="submit" class="btn btn-secondary"><a href="/contact">contact</a></button>
          <button type="submit" class="btn btn-secondary"><a href="/news-article">news & press</a></button>
      </ul>
-->
  </div>
</div>
</div>
<div class="copyright">
Copyright &copy; 2006-2017 Park & Caporrino - All Rights Reserved</div>
</footer>
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/media.css">
<?php wp_footer(); ?>

</body>
</html>
