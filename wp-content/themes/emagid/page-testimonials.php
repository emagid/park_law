<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class='jumbotron' id="page_about" style="background-image: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/banner_pa_1.jpg);">
      <h1><?php the_title(); ?></h1>
    </div>

<div class="container_test">
    

    <div class=" testimonials" id="testimonials">
            <h1><?php the_field('testimonials'); ?></h1>
        <div class="review_slider">
        
            <?php
            $args = array(
      'post_type' => 'testimonials'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
        
    <div class="review_block">
        <div class="review_image">
            <img src="<?php the_field('image'); ?>" alt="..." />
            <p><?php the_field('name'); ?></p>
            <img class="stars" src="<?php echo get_bloginfo('template_url'); ?>/assets/img/fivestars.png" alt="..." />
        </div>
        <div class="review_text">
            <p><?php the_field('testimonial'); ?></p>
        </div>
    </div>
            <?php
            }
                  }
            else  {
            echo 'No Reviews Found';
            }
      ?> 
    </div>
        </div>


 
    </div>
    <div>

</div>


<div class="areaofexpertise" id="">
  <div class="container">
    <h1>Areas of Practice</h1>
    <hr class="style1">
  </div>
    <div class="container area">

        <div class="row areas_expertise">
              <div class="col-xs-12 col-md-4">
        <div class="practice_area" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/img/family_law.jpg);">
        <a href="/practice-areas/family-law/">
            <div class="overlay">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/family.png" />
                <p>Family Law</p>
            </div>
        </a>
    </div>
                  
                  
                      
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
              <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/real_estate_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('real_estate_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
               <div class="col-xs-12 col-md-4">
                  <a href="/practice-areas" class="thumbnail">
                  <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/bankruptcy_thumb.jpg" alt="..." />
                  
<!--
                  <div class="inside">
                      <h3><?php the_field('bankruptcy_box'); ?></h3>
                  </div>
-->
            <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                      </a>
              </div>
        </div>
        

    </div>
        <div class="clear_row">
        
            <a class="btn btn-primary btn-lg view_btn" href="/practice-areas/" role="button" tabindex="0">View All Areas</a>
        </div>

   

<!--Slick Slider-->
<script>
$(document).ready(function(){
  $('.review_slider').slick({
      autoplay: true,
      autoplaySpeed: 3000,
                 dots: true,
            infinite: true,
            speed: 500,
    arrows: true
  });
});
</script>


<?php
get_footer();
