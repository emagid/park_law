<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

    <div class='jumbotron' id="page_areas" style="background-image: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/banner_news.jpg);">
    </div>
    
<section>

<div class='banner'><h2><?php the_title(); ?></h2></div>
</section>

<div class="tab_sidebar">
    <ul>
        <li data-related="bankrupt">Bankruptcy</li>
        <li data-related="business">Business Law</li>
        <li data-related="civil">Civil Litigation</li>
        <li data-related="criminal">Criminal Law</li>
        <li data-related="family">Family Law</li>
        <li data-related="landlord">Landlord - Tenant Practice</li>
        <li data-related="real_estate">Real Estate</li>
        <li data-related="tickets">Tickets & DWI </li>
        <li data-related="planning">Wills & Estate Planning </li>
    </ul>
</div>
    <section class='welcome' id="blog_posts">
                      <div class='container articles' id="main" style="display:block;">
          <h3>All News & Press</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
        
              <div class='container articles' id="bankrupt">
          <h3>Bankruptcy News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="bankruptcy" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="business">
          <h3>Business Law News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="business-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="civil">
          <h3>Civil Litigation News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="civil-litigation" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="criminal">
          <h3>Criminal Law News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="criminal-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="landlord">
          <h3>Landlord - Tenant Practice News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="domestic-violence" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
              <div class='container articles' id="family">
          <h3>Family Law News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="family-law" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>

                      <div class='container articles' id="tickets">
          <h3>Tickets & DWI News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="juvenile-delinquency" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
      </div>
                      <div class='container articles' id="planning">
          <h3>Wills & Estate Planning News</h3>
          <div class="article_post">
            <?php echo do_shortcode('[display-posts category="motor-vehicle-offenses" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
           </div>
                 
        </div>
            <div class='container articles' id="real_estate">
              <h3>Real Estate News</h3>
              <div class="article_post">
                <?php echo do_shortcode('[display-posts category="real-estate" include_date="true" date_format="F j, Y" include_author="true" include_excerpt="true" excerpt_more=".... Read More" excerpt_more_link="true"]'); ?>
               </div>
          </div>

    </section>
<script>
$(".tab_sidebar ul li").on("click", function() {
    $(".tab_sidebar ul li").removeClass('active');
    $(this).addClass('active');
    $('section#blog_posts div.container').hide();
  $("div[id=" + $(this).attr("data-related") + "]").show();
});
</script>

<?php
get_footer();
