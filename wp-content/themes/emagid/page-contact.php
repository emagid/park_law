<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<section class="contact_page_container">
    <div class="contact_maps">
        <iframe class="map_nj" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3017.3390922757394!2d-73.99551838499995!3d40.864432079315556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f7254299ef25%3A0x92129109a87aca1f!2s130+Fort+Lee+Rd%2C+Leonia%2C+NJ+07605!5e0!3m2!1sen!2sus!4v1501602253390" width="100%" height="750px" frameborder="0" style="border:0" allowfullscreen></iframe>
        <iframe class="map_ny" style="display:none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.700097008235!2d-73.98761584900471!3d40.746624179226636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259a867d19a55%3A0xabd4e4b9e9208422!2s307+5th+Ave%2C+New+York%2C+NY+10016!5e0!3m2!1sru!2sus!4v1499374189405" width="100%" height="750px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>


    <div class="contact_text">
        <div class="areaofexpertise">
        

        <div class="container">
            <h1>Contact Us</h1>
            <hr class="style1">
        </div>
            
            <div class="contact_switcher">
                <div class="switch selected" id="switch_location">
                    <h4>Locations</h4>
                </div>
                <div class="switch" id="switch_contact">
                    <h4>Contact</h4>
                </div>
                
            </div>
            
            <div class="contact_area">
                <div class="contact_addresses">
                    <div class="address selected" id="address_nj">
                        <p>130 Fort Lee Road<br>
                        Leonia, NJ 07605<br>
                        (201) 242-9666 (Andrew/Jill Park)<br>
                        (201) 592-7767 (James Caporrino)</p>
                    </div>
                    <div class="address" id="address_ny">
                        <p>307 5th Avenue, 4th Floor <br>
                        New York, NY 10016<br>
                        (212) 696-9494
                        </p>
                    </div>
                    
                    <div class="contact_email">
                        <p>
                        <a href="mailto:jsp@parkcaporrino.com">Jill SunJung Park – jsp@parkcaporrino.com</a></p>
                        <p>
                        <a href="mailto:JAC@PARKCAPORRINO.COM">James A. Caporrino – jac@parkcaporrino.com</a>  </p> 
                        <p>
                        <a href="mailto:AIP@PARKCAPORRINO.COM">Andrew Inbeh Park – aip@parkcaporrino.com</a></p>
                        <p>
                        <a href="mailto:TMP@PARKCAPORRINO.COM">Ted Minsung Park – tmp@parkcaporrino.com</a>     </p>            
                    </div>
                </div>
                <div class="contact_form" style="display:none;">
                <?php echo do_shortcode('[contact-form-7 id="32" title="Contact Form"]'); ?>
                </div>
            
            </div>
            
        </div>
    </div>
</section>


<script>
$(".contact_area .address").on("click", function() {
    $(".contact_area .address").removeClass('selected');
    $(this).addClass('selected');
});
    
$("div#address_ny").on("click", function() {
    $(".map_nj").hide();
    $(".map_ny").show();
});
    
$("div#address_nj").on("click", function() {
    $(".map_ny").hide();
    $(".map_nj").show();
});
    
$(".contact_switcher .switch").on("click", function() {
    $(".contact_switcher .switch").removeClass('selected');
    $(this).addClass('selected');
});
    
$("div#switch_location").on("click", function() {
    $(".contact_form").hide();
    $(".contact_addresses").show();
});
    
$("div#switch_contact").on("click", function() {
    $(".contact_addresses").hide();
    $(".contact_form").show();
});
</script>


<?php
get_footer();
